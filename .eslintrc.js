module.exports = {
  env: {
    commonjs: true,
    es2020: true,
    node: true,
    jest: true,
  },
  parser: "babel-eslint",
  extends: ["airbnb-base", "prettier"],
  parserOptions: {
    ecmaVersion: 11,
  },
  rules: {
    semi: ["error", "always"],
    quotes: ["error", "double"],
    "class-methods-use-this": "off",
    "no-empty-function": "off",
    "import/prefer-default-export": "off",
    camelcase: "off",
  },
};
