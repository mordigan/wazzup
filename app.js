const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const { swaggerDocs, swaggerUi } = require("./utils/swagger");

const notesRouter = require("./routes/notes");
const usersRouter = require("./routes/users");

const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/notes", notesRouter);
app.use("/users", usersRouter);
app.use("/", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

module.exports = app;
