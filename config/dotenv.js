const dotenv = require("dotenv").config();

if (dotenv.errors) {
  process.exit(1);
}
