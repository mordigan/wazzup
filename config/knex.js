require("./dotenv");

const env = process.env.NODE_ENV;
const knex = require("knex");
const knexFile = require("../knexfile")[env];

module.exports = {
  knex: knex({ ...knexFile }),
  config: knexFile.connection,
};
