const uuid = require("uuid");
const { Note, SharedNote } = require("../models");
const Pager = require("../utils/Pager");

const getAll = async (cursor, limit, page, user_id) => {
  try {
    let limits = { limit: limit || 10, page: page || 0 };
    if (cursor) limits = Pager.decoder(cursor);
    const result = await Pager.pager(Note, limits, { user_id });
    return { status: 200, result };
  } catch (e) {
    return { status: 500, result: e.message };
  }
};

const createNote = async (body, user_id) => {
  try {
    const note = new Note({ ...body, user_id });
    if (!note.valid) return { status: 422, result: note.validationIssues };
    const status = await note.save();
    if (status) return { status: 200, result: note };
    return { status: 422, result: note.error };
  } catch (e) {
    return { status: 500, result: e.message };
  }
};

const getOne = async (id, user_id) => {
  try {
    const note = await new Note({}).findOneWhere({ id, user_id });
    if (!note) return { status: 404, result: "Note not found" };
    return { status: 200, result: note };
  } catch (e) {
    return { status: 500, result: e.message };
  }
};

const deleteOne = async (id, user_id) => {
  try {
    const note = await new Note({}).findOneWhere({ id, user_id });
    if (!note) return { status: 404, result: "Note not found" };
    const shared = await new SharedNote({}).findWhere({ note_id: note.id });
    shared.forEach(async (i) => {
      await i.destroy();
    });
    const s = await note.destroy();
    if (s) return { status: 200, result: note };
    return { status: 422, result: note.error };
  } catch (e) {
    return { status: 500, result: e.message };
  }
};

const updateOne = async (id, user_id, body) => {
  try {
    const note = await new Note({}).findOneWhere({ id, user_id });
    if (!note) return { status: 404, result: "Note not found" };
    const s = await note.update({ ...body });
    if (s) return { status: 200, result: note };
    return { status: 422, result: note.error };
  } catch (e) {
    return { status: 500, result: e.message };
  }
};

const shareOne = async (id, user_id) => {
  try {
    const note = await new Note({}).findOneWhere({ id, user_id });
    if (!note) return { status: 404, result: "Note not found" };
    const shared = new SharedNote({
      note_id: note.id,
      unique_id: uuid.v4(),
      user_id,
    });
    if (!shared.valid) return { status: 422, result: shared.validationIssues };
    const status = await shared.save();
    if (status) return { status: 200, result: shared };
    return { status: 422, result: shared.error };
  } catch (e) {
    return { status: 500, result: e.message };
  }
};

const getOneShared = async (unique_id) => {
  try {
    const sharedNote = await new SharedNote({}).findOneWhere({ unique_id });
    if (!sharedNote) return { status: 404, result: "Note not found" };
    const note = await new Note({}).findById(sharedNote.note_id);
    if (!note) return { status: 404, result: "Note not found" };
    return { status: 200, result: note };
  } catch (e) {
    return { status: 500, result: e.message };
  }
};

const getAllSelfShared = async (limit, page, cursor, user_id) => {
  try {
    let limits = { limit: limit || 10, page: page || 0 };
    if (cursor) limits = Pager.decoder(cursor);
    const result = await Pager.pager(SharedNote, limits, { user_id });
    return { status: 200, result };
  } catch (e) {
    return { status: 500, result: e.message };
  }
};

module.exports = {
  getAll,
  createNote,
  getOne,
  deleteOne,
  updateOne,
  shareOne,
  getOneShared,
  getAllSelfShared,
};
