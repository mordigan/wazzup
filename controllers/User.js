const { User, UserSession } = require("../models");
const { compare } = require("../utils/bcrypt");
const { createToken } = require("../middleware/jwtGen");

const registration = async (user) => {
  try {
    const u = new User(user);
    if (!u.valid) return { status: 422, result: u.validationIssues };
    const s = await u.save();
    if (s) return { status: 200, result: "Created" };
    return { status: 422, result: u.error };
  } catch (e) {
    return { status: 500, result: "Internal Server Error" };
  }
};

const login = async (email, password) => {
  try {
    const u = await new User({}).findOneWhere({ email });
    if (!u) return { status: 404, result: "User not found" };
    const same = await compare(password, u.password);
    if (!same) return { status: 422, result: "Incorect password" };
    const jwt = await createToken(u.id, u.email);
    return { status: 200, result: jwt };
  } catch (e) {
    return { status: 500, result: "Internal Server Error" };
  }
};

const logout = async (everywhere, session, user) => {
  try {
    if (!everywhere) {
      await session.update({ is_revoked: true });
    } else {
      const sessions = await new UserSession({}).findWhere({
        user_id: user.id,
      });
      if (!sessions.length) return 404;
      await Promise.all(
        sessions.map(async (s) => {
          await s.update({ is_revoked: true });
        })
      );
    }
    return 200;
  } catch (e) {
    return 500;
  }
};

module.exports = { registration, login, logout };
