const jwt = require("jsonwebtoken");
const { User, UserSession } = require("../models");

const auth = (req, res, next) => {
  const token =
    req.body.token ||
    req.query.token ||
    req.headers["x-access-token"] ||
    req.cookies.token;
  if (!token) return res.status(401).json("Unauthorized");

  // eslint-disable-next-line consistent-return
  return jwt.verify(token, process.env.JWT_SECRET, async (err, decode) => {
    if (err) return res.status(422).json(err.message);
    req.userSession = await new UserSession({}).findOneWhere({
      token,
      user_id: decode.id,
      is_revoked: false,
    });

    if (!req.userSession) return res.status(401).json("Unauthorized");

    req.user = await new User({}).findById(req.userSession.user_id);
    delete req.user.password;
    return next();
  });
};

module.exports = { auth };
