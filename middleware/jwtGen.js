const jwt = require("jsonwebtoken");
const { UserSession } = require("../models");

const createToken = async (id, email) => {
  const token = jwt.sign(
    { id, email, signInDate: new Date() },
    process.env.JWT_SECRET,
    {
      expiresIn: "1d",
    }
  );

  const userSession = new UserSession({ user_id: id, token, ttl: 24 });
  const s = await userSession.save();
  if (s) return userSession;
  return null;
};

module.exports = { createToken };
