exports.up = async (knex) => {
  return knex.schema.createTable("users", (t) => {
    t.increments("id").notNullable();
    t.string("email").notNullable();
    t.string("password").notNullable();
    t.unique(["email"]);
    t.index("email");
    t.timestamp("created_at").defaultTo(knex.fn.now());
    t.timestamp("updated_at").defaultTo(knex.fn.now());
  });
};

exports.down = async (knex) => {
  return knex.schema.dropTable("users");
};
