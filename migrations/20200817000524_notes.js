exports.up = async (knex) => {
  return knex.schema.createTable("notes", (t) => {
    t.increments("id").notNullable();
    t.string("note", 1000).notNullable().defaultTo("");
    t.bigInteger("user_id")
      .unsigned()
      .notNullable()
      .references("id")
      .inTable("users");
    t.index("user_id");
    t.timestamp("created_at").defaultTo(knex.fn.now());
    t.timestamp("updated_at").defaultTo(knex.fn.now());
  });
};

exports.down = async (knex) => {
  return knex.schema.dropTable("notes");
};
