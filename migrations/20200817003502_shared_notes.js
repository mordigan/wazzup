exports.up = async (knex) => {
  return knex.schema.createTable("shared_notes", (t) => {
    t.increments("id").notNullable();
    t.bigInteger("note_id").notNullable().unsigned();
    t.bigInteger("user_id").notNullable().unsigned();
    t.foreign("note_id").references("id").inTable("notes").onDelete("cascade");
    t.foreign("user_id").references("id").inTable("users").onDelete("cascade");
    t.uuid("unique_id").notNullable();
    t.index(["note_id"]);
    t.index(["user_id"]);
    t.unique("note_id");
    t.index(["unique_id"]);
    t.timestamp("created_at").defaultTo(knex.fn.now());
    t.timestamp("updated_at").defaultTo(knex.fn.now());
  });
};

exports.down = async (knex) => {
  return knex.schema.dropTable("shared_notes");
};
