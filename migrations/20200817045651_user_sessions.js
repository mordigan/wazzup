exports.up = async (knex) => {
  return knex.schema.createTable("user_sessions", (t) => {
    t.increments("id").notNullable();
    t.bigInteger("user_id")
      .unsigned()
      .notNullable()
      .references("id")
      .inTable("users");
    t.string("user_agent");
    t.string("token");
    t.boolean("is_revoked").notNullable().defaultTo(false);
    t.integer("ttl");
    t.index(["user_id"]);
    t.index(["token"]);
    t.index(["is_revoked"]);
    t.timestamp("created_at").defaultTo(knex.fn.now());
    t.timestamp("updated_at").defaultTo(knex.fn.now());
  });
};

exports.down = async (knex) => {
  return knex.schema.dropTable("user_sessions");
};
