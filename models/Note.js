const Model = require("./model");

class Note extends Model {
  static name = "Note";

  constructor(props) {
    super(props);
    this.table = "notes";
  }

  validate() {
    if (!this.note) this.addValidationIssue("[note] is missing");
    else if (this.note.length > 1000)
      this.addValidationIssue("[note] too long(max 1000 characters)");
    const possibleFields = [
      "id",
      "note",
      "user_id",
      "created_at",
      "updated_at",
    ];
    Object.keys(this).forEach((k) => {
      if (!possibleFields.includes(k))
        this.addValidationIssue(`Unknown parameter ${k}`);
    });
    if (this.validationIssues.length) super.validate();
  }

  newModel(args) {
    return new Note(args);
  }
}

module.exports = Note;
