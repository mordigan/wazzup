const Model = require("./model");

class SharedNote extends Model {
  static name = "SharedNote";

  constructor(props) {
    super(props);
    this.table = "shared_notes";
  }

  validate() {
    if (!this.note_id) this.addValidationIssue("[note_id] is missing");
    if (!this.unique_id) this.addValidationIssue("[unique_id] is missing");
    if (!this.user_id) this.addValidationIssue("[user_id] is missing");
    const possibleFields = [
      "id",
      "note_id",
      "user_id",
      "unique_id",
      "created_at",
      "updated_at",
    ];
    Object.keys(this).forEach((k) => {
      if (!possibleFields.includes(k))
        this.addValidationIssue(`Unknown parameter ${k}`);
    });
    if (this.validationIssues.length) super.validate();
  }

  newModel(args) {
    return new SharedNote(args);
  }
}

module.exports = SharedNote;
