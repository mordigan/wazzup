const Model = require("./model");
const { bcrypt } = require("../utils/bcrypt");

class User extends Model {
  static name = "User";

  validate() {
    if (!this.email) this.addValidationIssue("[email] is missing");
    else {
      const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!this.email.match(regex))
        this.addValidationIssue("Please provide a valid [email](****@***.**)");
    }
    if (!this.password) this.addValidationIssue("[password] is missing");
    else {
      if (this.password.length < 8)
        this.addValidationIssue("[password] is too short(min 8 characters)");
      if (this.password.length > 128)
        this.addValidationIssue("[password] is too long(max 128 characters)");
    }
    const possibleFields = [
      "email",
      "password",
      "id",
      "created_at",
      "updated_at",
    ];
    Object.keys(this).forEach((k) => {
      if (!possibleFields.includes(k))
        this.addValidationIssue(`Unknown parameter ${k}`);
    });
    if (this.validationIssues.length) super.validate();
  }

  constructor(args) {
    super(args);
    this.table = "users";
  }

  async beforeCreate() {
    this.password = bcrypt.hashSync(
      this.password,
      Number(process.env.SALT_ROUNDS)
    );
  }

  newModel(args) {
    return new User(args);
  }
}

module.exports = User;
