const Model = require("./model");

class UserSession extends Model {
  static name = "UserSession";

  validate() {
    const possibleFields = [
      "user_agent",
      "user_id",
      "id",
      "token",
      "is_revoked",
      "ttl",
      "created_at",
      "updated_at",
    ];
    Object.keys(this).forEach((k) => {
      if (!possibleFields.includes(k))
        this.addValidationIssue(`Unknown parameter ${k}`);
    });
    if (this.validationIssues.length) super.validate();
  }

  constructor(args) {
    super(args);
    this.table = "user_sessions";
  }

  newModel(args) {
    return new UserSession(args);
  }
}

module.exports = UserSession;
