const path = require("path");
const fs = require("fs");

const basename = path.basename(__filename);

const db = {
  Note: undefined,
  User: undefined,
  UserSession: undefined,
  SharedNote: undefined,
};
fs.readdirSync(__dirname)
  .filter(
    (file) =>
      file.indexOf(".") !== 0 &&
      file !== basename &&
      file !== "model.js" &&
      file.slice(-3) === ".js"
  )
  .forEach((file) => {
    // eslint-disable-next-line global-require,import/no-dynamic-require
    const model = require(path.join(__dirname, file));
    db[model.name] = model;
  });

module.exports = db;
