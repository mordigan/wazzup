const { knex } = require("../config/knex");

class Model {
  #table = "";

  #valid = true;

  #validationIssues = [];

  constructor(args) {
    this.beforeInitialization();
    Object.keys(args).forEach((n) => {
      this[n] = args[n];
    });
    this.validate();
    this.afterInitialization();
  }

  get table() {
    return this.#table;
  }

  set table(tableName) {
    this.#table = tableName;
  }

  get valid() {
    return this.#valid;
  }

  get validationIssues() {
    return this.#validationIssues;
  }

  async findById(id) {
    try {
      const f = await knex(this.#table).where({ id }).first();
      return this.newModel(f);
    } catch (e) {
      this.setError(e);
      return null;
    }
  }

  async findWhere(args, limit = 500, offset = 0, order) {
    try {
      // eslint-disable-next-line no-param-reassign
      if (!order) order = [{ column: "id", order: "asc" }];
      const res = await knex(this.#table)
        .where(args)
        .orderBy(order)
        .limit(limit)
        .offset(offset);
      return res.map((i) => this.newModel(i));
    } catch (e) {
      this.setError(e);
      return [];
    }
  }

  async findOneWhere(args) {
    try {
      const [res] = await knex(this.#table).where(args);
      return this.newModel(res);
    } catch (e) {
      this.setError(e);
      return null;
    }
  }

  async findOneWhereLast(args) {
    try {
      const [res] = await knex(this.#table).where(args).orderBy("id", "desc");
      return this.newModel(res);
    } catch (e) {
      this.setError(e);
      return null;
    }
  }

  async update(args) {
    try {
      if (!this.#valid) return false;
      await this.beforeUpdate();
      await knex(this.#table)
        .where({ id: this.id })
        .update({ ...args, updated_at: knex.fn.now() });
      await this.afterUpdate();
      return true;
    } catch (e) {
      this.setError(e);
      return false;
    }
  }

  async destroy() {
    try {
      await this.beforeDestroy();
      await knex(this.#table).where({ id: this.id }).delete();
      await this.afterDestroy();
      return true;
    } catch (e) {
      this.setError(e);
      return false;
    }
  }

  async save() {
    try {
      if (!this.#valid) return false;
      await this.beforeCreate();
      await knex(this.#table).insert(this);
      await this.afterCreate();
      return true;
    } catch (e) {
      this.setError(e);
      return false;
    }
  }

  // eslint-disable-next-line no-unused-vars
  newModel(args) {}

  async beforeUpdate() {}

  async afterUpdate() {
    const self = await this.findById(this.id);
    Object.keys(self).forEach((k) => {
      this[k] = self[k];
    });
  }

  async beforeCreate() {}

  async afterCreate() {
    const self = await this.findOneWhereLast(this);
    Object.keys(self).forEach((k) => {
      this[k] = self[k];
    });
  }

  async beforeDestroy() {}

  async afterDestroy() {
    Object.keys(this).forEach((k) => {
      this[k] = null;
    });
  }

  validate() {
    this.#valid = false;
  }

  beforeInitialization() {}

  afterInitialization() {}

  setError(e) {
    this.error = e.message;
  }

  addValidationIssue(issue) {
    this.#validationIssues = [...this.#validationIssues, issue];
  }
}

module.exports = Model;
