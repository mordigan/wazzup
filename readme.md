[![forthebadge](https://forthebadge.com/images/badges/check-it-out.svg)]()
[![forthebadge](https://forthebadge.com/images/badges/ages-18.svg)]()
[![forthebadge](https://forthebadge.com/images/badges/built-by-developers.svg)]()
[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)]()
[![forthebadge](https://forthebadge.com/images/badges/built-with-swag.svg)]()
[![forthebadge](https://forthebadge.com/images/badges/contains-technical-debt.svg)]()
[![forthebadge](https://forthebadge.com/images/badges/gluten-free.svg)]()
[![forthebadge](https://forthebadge.com/images/badges/made-with-javascript.svg)]()
[![forthebadge](https://forthebadge.com/images/badges/powered-by-electricity.svg)]()

# How to start

- `npm ci` to install the dependencies
- `npm run db:migrate` after you enter the database connection credentials(if on development or test environments an sqlite3 file will be created for corresponding environment)
- `npm test` to test
- `npm run start:dev` to run in node monitor
- `npm start` to start
