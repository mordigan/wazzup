/**
 * @swagger
 * definitions:
 *   User:
 *     type: object
 *     required:
 *       - email
 *       - password
 *     properties:
 *       email:
 *         type: string
 *       password:
 *         type: string
 *   Note:
 *     type: object
 *     required:
 *       - note
 *     properties:
 *       note:
 *         type: string
 *   PaginationQuery:
 *     type: object
 *     properties:
 *       cursor:
 *         type: string
 *       limit:
 *         type: number
 *       page:
 *         type: number
 *   LogOutBody:
 *     type: object
 *     properties:
 *       everywhere:
 *         type: boolean
 */
