const express = require("express");
const Note = require("../controllers/Note");
const { auth } = require("../middleware/jwtAuth");

const router = express.Router();

/**
 * @swagger
 * /notes:
 *   get:
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *       - in: body
 *         name: pagination
 *         schema:
 *           $ref: '#/definitions/PaginationQuery'
 *     description: Endpoint to see paginated notes
 *     responses:
 *       '200':
 *         description: A paginated notes response
 *       '500':
 *         description: Internal Server Error
 */
router.get("/", auth, async (req, res) => {
  const { cursor, limit, page } = req.body;
  const { status, result } = await Note.getAll(
    cursor,
    limit,
    page,
    req.user.id
  );
  return res.status(status).json(result);
});

/**
 * @swagger
 * /notes:
 *   post:
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *       - in: body
 *         name: note
 *         schema:
 *           $ref: '#/definitions/Note'
 *     description: Endpoint to create a note
 *     responses:
 *       '200':
 *         description: The created note object
 *       '500':
 *         description: Internal Server Error
 *       '422':
 *         description: Unprocessable entity
 */
router.post("/", auth, async (req, res) => {
  const { status, result } = await Note.createNote(req.body, req.user.id);
  return res.status(status).json(result);
});

/**
 * @swagger
 * /notes/{id}:
 *   patch:
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: number
 *       - in: body
 *         name: note
 *         schema:
 *           $ref: '#/definitions/Note'
 *     description: Endpoint to update a note
 *     responses:
 *       '200':
 *         description: The updated note object
 *       '500':
 *         description: Internal Server Error
 *       '422':
 *         description: Unprocessable entity
 *       '404':
 *         description: Note not found
 */
router.patch("/:id", auth, async (req, res) => {
  const { status, result } = await Note.updateOne(
    req.params.id,
    req.user.id,
    req.body
  );
  return res.status(status).json(result);
});

/**
 * @swagger
 * /notes/shared:
 *   get:
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *       - in: body
 *         name: pagination
 *         schema:
 *           $ref: '#/definitions/PaginationQuery'
 *     description: Endpoint to see a note
 *     responses:
 *       '200':
 *         description: The existing note object
 *       '500':
 *         description: Internal Server Error
 *       '404':
 *         description: Note not found
 */
router.get("/shared", auth, async (req, res) => {
  const { limit, page, cursor } = req.body;
  const { status, result } = await Note.getAllSelfShared(
    limit,
    page,
    cursor,
    req.user.id
  );
  return res.status(status).json(result);
});

/**
 * @swagger
 * /notes/{id}:
 *   get:
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: number
 *     description: Endpoint to see a note
 *     responses:
 *       '200':
 *         description: The existing note object
 *       '500':
 *         description: Internal Server Error
 *       '404':
 *         description: Note not found
 */
router.get("/:id", auth, async (req, res) => {
  const { status, result } = await Note.getOne(req.params.id, req.user.id);
  return res.status(status).json(result);
});

/**
 * @swagger
 * /notes/{id}:
 *   delete:
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: number
 *     description: Endpoint to delete a note
 *     responses:
 *       '200':
 *         description: The deleted note object
 *       '500':
 *         description: Internal Server Error
 *       '422':
 *         description: Unprocessable entity
 *       '404':
 *         description: Note not found
 */
router.delete("/:id", auth, async (req, res) => {
  const { status, result } = await Note.deleteOne(req.params.id, req.user.id);
  return res.status(status).json(result);
});

/**
 * @swagger
 * /notes/share/{id}:
 *   get:
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: number
 *     description: Endpoint to make a note public
 *     responses:
 *       '200':
 *         description: The shared note object
 *       '500':
 *         description: Internal Server Error
 *       '404':
 *         description: Note not found
 *       '422':
 *         description: Unprocessable Entity
 */
router.post("/share/:id", auth, async (req, res) => {
  const { status, result } = await Note.shareOne(req.params.id, req.user.id);
  return res.status(status).json(result);
});

/**
 * @swagger
 * /notes/shared/{uuid}:
 *   get:
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: uuid
 *         required: true
 *         schema:
 *           type: string
 *           format: uuid
 *     description: Endpoint to delete a note
 *     responses:
 *       '200':
 *         description: The shared note object
 *       '500':
 *         description: Internal Server Error
 *       '404':
 *         description: Note not found
 */
router.get("/shared/:uuid", async (req, res) => {
  const { status, result } = await Note.getOneShared(req.params.uuid);
  return res.status(status).json(result);
});

module.exports = router;
