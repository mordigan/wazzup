const express = require("express");
const User = require("../controllers/User");
const { auth } = require("../middleware/jwtAuth");

const router = express.Router();
/**
 * @swagger
 * /users/register:
 *   post:
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: user
 *         description: User to create
 *         schema:
 *           $ref: '#/definitions/User'
 *     description: Endpoint to register a user
 *     responses:
 *       '200':
 *         description: Created
 *       '500':
 *         description: Internal Server Error
 *       '422':
 *         description: Unprocessable Entity(some parameters are wrong)
 */
router.post("/register", async (req, res) => {
  const { status, result } = await User.registration(req.body);
  return res.status(status).json(result);
});

/**
 * @swagger
 * /users/login:
 *   post:
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: user
 *         description: User to create
 *         schema:
 *           $ref: '#/definitions/User'
 *     description: Endpoint to log in a user
 *     responses:
 *       '200':
 *         description: A User Session Object
 *       '500':
 *         description: Internal Server Error
 *       '422':
 *         description: Incorrect password
 *       '404':
 *         description: User with this email is not found
 */
router.post("/login", async (req, res) => {
  const { status, result } = await User.login(
    req.body.email,
    req.body.password
  );
  return res.status(status).json(result);
});

/**
 * @swagger
 * /users/currentUser:
 *   get:
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *     description: Endpoint to see the user information provided the JWT("x-access-token" header, cookie or body)
 *     responses:
 *       '200':
 *         description: A User object
 *       '401':
 *         description: Token expired or doesn't exist
 *       '422':
 *         description: Incorrect token
 */
router.get("/currentUser", auth, async (req, res) => {
  res.json(req.user);
});

/**
 * @swagger
 * /users/logout:
 *   post:
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *       - in: body
 *         name: everywhere
 *         schema:
 *           $ref: '#/definitions/Note'
 *     description: Endpoint to end session
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Token expired or doesn't exist
 *       '404':
 *         description: Session not found
 *       '500':
 *         description: Internal server error
 */
router.post("/logout", auth, async (req, res) => {
  const action = await User.logout(
    req.body.everywhere,
    req.userSession,
    req.user
  );
  return res.sendStatus(action);
});

module.exports = router;
