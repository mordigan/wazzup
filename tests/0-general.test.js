const { registration, login, logout } = require("../controllers/User");
const {
  getAllSelfShared,
  getOneShared,
  getOne,
  getAll,
  shareOne,
  updateOne,
  deleteOne,
  createNote,
} = require("../controllers/Note");

const mockUser = {
  email: "test@test.com",
  password: "Testing1234",
};

let uuid = "";
let userId = 0;
let userSession = {};

describe("Users can", () => {
  test("'t register without email", async (done) => {
    const { status } = await registration({ password: "Testing1234" });
    expect(status).toBe(422);
    done();
  });
  test("'t register without password", async (done) => {
    const { status } = await registration({
      email: "test@test.com",
    });
    expect(status).toBe(422);
    done();
  });
  test("register", async (done) => {
    const { status } = await registration(mockUser);
    expect(status).toBe(200);
    done();
  });
  test("'t log in with wrong password", async (done) => {
    const { status } = await login(mockUser.email, `${mockUser.password}1`);
    expect(status).toBe(422);
    done();
  });
  test("'t log in with wrong email", async (done) => {
    const { status } = await login(`${mockUser.email}1`, mockUser.password);
    expect(status).toBe(404);
    done();
  });
  test("log in", async (done) => {
    const { status, result } = await login(mockUser.email, mockUser.password);
    userId = result.user_id;
    userSession = result;
    expect(status).toBe(200);
    done();
  });
  test("'t create a note with empty body", async (done) => {
    const { status } = await createNote({ note: "" }, userId);
    expect(status).toBe(422);
    done();
  });
  test("create a note", async (done) => {
    const { status } = await createNote({ note: "Testing Note" }, userId);
    expect(status).toBe(200);
    done();
  });
  test("get a note", async (done) => {
    const { status } = await getOne(1, userId);
    expect(status).toBe(200);
    done();
  });
  test("get all note", async (done) => {
    const { status } = await getAll("", 10, 0, userId);
    expect(status).toBe(200);
    done();
  });
  test("get all shared note", async (done) => {
    const { status } = await getAllSelfShared(0, 10, "", userId);
    expect(status).toBe(200);
    done();
  });
  test("update a note", async (done) => {
    const { status } = await updateOne(1, userId, { note: "Testing Note 1" });
    expect(status).toBe(200);
    done();
  });
  test("share a note", async (done) => {
    const { status, result } = await shareOne(1, userId);
    uuid = result.unique_id;
    expect(status).toBe(200);
    done();
  });
  test("get a shared note", async (done) => {
    const { status } = await getOneShared(uuid);
    expect(status).toBe(200);
    done();
  });
  test("'t get a shared note with wrong uuid", async (done) => {
    const { status } = await getOneShared(`${uuid}1`);
    expect(status).toBe(404);
    done();
  });
  test("delete a note", async (done) => {
    const { status } = await deleteOne(1, userId);
    expect(status).toBe(200);
    done();
  });
  test("log out", async (done) => {
    const status = await logout(true, userSession, { id: userId });
    expect(status).toBe(200);
    done();
  });
});
