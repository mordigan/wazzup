class Pager {
  static encoder = (
    object = {
      limit: 10,
      page: 1,
    }
  ) => Buffer.from(JSON.stringify(object)).toString("base64");

  static decoder = (cursor) =>
    JSON.parse(Buffer.from(cursor, "base64").toString("utf8"));

  static pager = async (
    Model,
    limits = { limit: 10, page: 0 },
    query,
    order
  ) => {
    const list = await new Model({}).findWhere(
      query,
      limits.limit,
      limits.limit * limits.page,
      order
    );
    const previousCursor = !limits.page
      ? ""
      : Pager.encoder({ ...limits, page: limits.page - 1 });
    const currentCursor = Pager.encoder(limits);
    const nextCursor =
      list.length === limits.limit
        ? Pager.encoder({ ...limits, page: limits.page + 1 })
        : "";
    return {
      list,
      limit: limits.limit,
      nextCursor,
      previousCursor,
      currentCursor,
    };
  };
}

module.exports = Pager;
