const bcrypt = require("bcrypt");
const { promisify } = require("util");

const compare = promisify(bcrypt.compare).bind(bcrypt);

module.exports = { bcrypt, compare };
