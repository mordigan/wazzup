const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "Wazzup tests",
      description: "Note API",
    },
    contact: {
      name: "Aram Hayrapetyan",
    },
    servers: ["http://127.0.0.1:3000"],
  },
  apis: ["./routes/*.js"],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

module.exports = { swaggerDocs, swaggerUi };
